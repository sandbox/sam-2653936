<?php
/**
 * @file
 * A style plugin to display a whole region inside a link.
 */

$plugin = [
  'title' => t('Link'),
  'description' => t('Display a pane or region with a link around it.'),
  'render region' => 'panels_link_style_render_region',
  'settings form' => 'panels_link_style_settings_form',
  'pane settings form' => 'panels_link_style_settings_form',
  'required context' => new ctools_context_required(t('Term'), 'taxonomy_term'),
  'edit form' => 'panels_link_style_settings_form',
];

/**
 * Settings form callback.
 */
function panels_link_style_settings_form($settings) {
  $form['link_style_field'] = [
    '#type' => 'textfield',
    '#title' => t('Link field'),
    '#description' => t('The page to link to'),
    '#default_value' => !empty($settings['link_style_field']) ? $settings['link_style_field'] : '',
  ];
  $form['link_classes'] = [
    '#type' => 'textfield',
    '#title' => t('Link Classes'),
    '#description' => t('Classes to add to the link.'),
    '#default_value' => !empty($settings['link_classes']) ? $settings['link_classes'] : '',
  ];
  return $form;
}

/**
 * Render callback for the region.
 */
function theme_panels_link_style_render_region($vars) {
  $output = '<div class="region region-' . $vars['region_id'] . '">';
  $pane_html = implode('', $vars['panes']);

  $classes = isset($vars['settings']['link_classes']) ? explode(' ', $vars['settings']['link_classes']) : '';
  // If we have a link, use it otherwise just the normal HTML.
  $link_style_url = ctools_context_keyword_substitute($vars['settings']['link_style_field'], [], $vars['display']->context);
  $output .= !empty($link_style_url) ? l($pane_html, $link_style_url, ['html' => TRUE, 'attributes' => ['class' => $classes]]) : $pane_html;

  $output .= '</div>';
  return $output;
}
